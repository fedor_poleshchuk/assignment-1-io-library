%define SYS_READ 0
%define SYS_WRITE 1
%define SYS_EXIT 60
%define STD_IN 0
%define STD_OUT 1

%define SPACE_SYMBOL 0x20
%define TAB_SYMBOL 0x9
%define LINEFEED_SYMBOL 0xA
%define CARRETURN_SYMBOL 0xD

%define ZERO_SYMBOL '0'
%define NINE_SYMBOL '9'

section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, SYS_EXIT
    xor rdi, rdi
    syscall

; Принимает указатель на нуль-терминированную строку (rdi), 
; Возвращает её длину (rax)
string_length:
    xor rax, rax
.loop:
    cmp byte [rdi + rax], 0
    je .exit
    inc rax
    jmp .loop
.exit:
    ret

; Принимает указатель на нуль-терминированную строку (rdi), 
; Выводит её в stdout
print_string:
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rdi, STD_OUT
    mov rax, SYS_WRITE
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, LINEFEED_SYMBOL

; Принимает код символа (rdi) и выводит его в stdout
print_char:
    push rdi
    mov rdx, 1
    mov rsi, rsp
    mov rdi, STD_OUT
    mov rax, SYS_WRITE
    syscall
    pop rdi
    ret

; Выводит знаковое 8-байтовое число (rdi) в десятичном формате
print_int:
    test rdi, rdi
    jns print_uint  ; беззнаковое -- печатаем как есть
    push rdi        ; иначе сначала печатаем '-'
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi         ; делаем беззнаковым
    ; и печатаем как беззнаковое сразу ниже

; Выводит беззнаковое 8-байтовое число (rdi) в десятичном формате
print_uint:
    mov rax, rdi
    mov rdi, rsp
    mov r8, 10

    ; подготовка буфера на стеке
    sub rsp, 24         ; максимум 20 цифр + нуль-терминатор, округлить до кратного 8
    dec rdi
    mov byte [rdi], 0   ; нуль-терминатор в конец буфера

.loop:
    xor rdx, rdx
    div r8              ; получаем [rax:rdx] <-> [частное:остаток]
    add dl, '0'         ; ASCII-код цифры 0-9 = цифра + '0' (= 0x30)
    dec rdi
    mov [rdi], dl
    test rax, rax
    jnz .loop

    call print_string   ; печатаем получившуюся строку
    add rsp, 24         ; освобождаем буфер и уходим
    ret

; Принимает два указателя на нуль-терминированные строки (rdi, rsi), 
; Возвращает (rax) 1 если они равны, 0 иначе
string_equals:
    xor rax, rax            ; по умолчанию строки не равны
    xor rcx, rcx            ; счётчик: сдвиг по строке
.loop:
    mov dl, byte[rdi + rcx] ; проверяем символы на равенство
    cmp dl, byte[rsi + rcx]
    jne .exit               ; если символы не равны -- уходим
    inc rcx
    test dl, dl             ; если равны -- проверяем на терминатор
    jne .loop
    inc rax                 ; если дошли до конца -- возвращаем 1
.exit:
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rdx, 1
    mov rsi, rsp
    mov rdi, STD_IN
    mov rax, SYS_READ
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера (rdi), размер буфера (rsi)
; Читает в буфер слово из stdin, пропуская пробельные символы (' ' = 0x20, '\t' = 0x9, CR = 0xD, LF = 0xA) в начале.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx. При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push r14

    ; инициализация и сохранение аргументов
    mov r12, rdi    ; адрес начала буфера
    mov r13, rsi    ; размер буфера
    xor r14, r14    ; длина прочитанного слова

.ws_skip:
    call read_char
    cmp al, SPACE_SYMBOL
    je .ws_skip
    cmp al, CARRETURN_SYMBOL
    je .ws_skip
    cmp al, LINEFEED_SYMBOL
    je .ws_skip
    cmp al, TAB_SYMBOL
    je .ws_skip
    test al, al  ; терминатор
    je .end_success

.word_read:
    mov byte[r12 + r14], al
    inc r14

    call read_char
    cmp al, SPACE_SYMBOL
    je .end_success
    cmp al, CARRETURN_SYMBOL
    je .end_success
    cmp al, LINEFEED_SYMBOL
    je .end_success
    cmp al, TAB_SYMBOL
    je .end_success
    test al, al     ; терминатор
    je .end_success
    cmp r14, r13    ; проверка размера
    je .end_fail

    jmp .word_read

.end_success:
    mov byte[r12 + r14], 0  ; допишем нуль-терминатор
    mov rax, r12
    mov rdx, r14
    jmp .end

.end_fail:
    xor rax, rax
    xor rdx, rdx

.end:
    mov rdi, r12    ; восстанавливаем аргументы
    mov rsi, r13
    pop r14
    pop r13
    pop r12
    ret


; Принимает указатель на строку (rdi), пытается прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov r8, 10
    xor rax, rax
    xor rcx, rcx

.loop:
    movzx r9, byte[rdi + rcx]
    cmp r9b, ZERO_SYMBOL
    jb .end
    cmp r9b, NINE_SYMBOL
    ja .end
    xor rdx, rdx
    mul r8
    sub r9b, ZERO_SYMBOL    ; '0' = 0x30
    add rax, r9
    inc rcx
    jmp .loop

.end:
    mov rdx, rcx
    ret


; Принимает указатель на строку (rdi), пытается прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    mov al, byte[rdi]
    cmp al, '-'
    je .signed

    cmp al, '+'         ; проверим и на '+' тоже (дельная идея из reivew)
    jne parse_uint

.signed:
    push rdi
    inc rdi
    call parse_uint
    pop r8

    test rdx, rdx       ; сначала проверить на ошибку
    jz .error

    xor r9, r9          ; необязательно, но пусть будет
    mov r9b, byte[r8]   ; число со знаком -- проверяем знак ещё раз
    cmp r9b, '-'
    jnz .positive
    neg rax
.positive:
    inc rdx
    ret

.error:
    xor rax, rax
    xor rdx, rdx
    ret

; Принимает указатель на строку (rdi), указатель на буфер (rsi) и длину буфера (rdx)
; Копирует строку в буфер. Возвращает (rax) длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi

    cmp rax, rdx
    jae .error

.loop:
    mov dl, byte[rdi]
    mov byte[rsi], dl
    inc rdi
    inc rsi
    test dl, dl
    jnz .loop

    mov rsi, rax
    ret

.error:
    xor rax, rax
    ret
